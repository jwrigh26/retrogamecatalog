//
//  NetworkManager.swift
//  RetroGameCatalog
//
//  Created by Justin Wright on 2/11/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import Alamofire

typealias networkHandler = (reponse: [AnyObject]?, error: NSError?) -> Void

class NetworkManager {
  
  
  static func logJSON(){
    let method = NetworkRouter.GetAllGames()
    
    NetworkConfigurator.sharedInstance.manager.request(method).responseJSON{
      response in
      
      print("Request \(response.request)")
      print("Response \(response.response)")
      print("Result \(response.result.value)")
      
    }
  }
  
  static func getItems(handler: networkHandler?){
    let method = NetworkRouter.GetAllGames()
    NetworkConfigurator.sharedInstance.manager.request(method).validate().responseObject{
      (response: Response<GameResponse, NSError>) in
      guard let value = response.result.value else {
        if let error = response.result.error {
          print("We have an error getting items")
          print(error)
        }
        handler?(reponse: nil, error: response.result.error)
        return
      }
      
      handler?(reponse: value.games, error: nil)
    }
    
  }
  
}
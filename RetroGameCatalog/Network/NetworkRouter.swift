//
//  NetworkRouter.swift
//  RetroGameCatalog
//
//  Created by Justin Wright on 2/11/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import Alamofire

enum NetworkRouter: URLRequestConvertible {
  
  static let baseUrl = "http://private-77367-retrogames.apiary-mock.com"
  
  case GetAllGames()
  
  var method: Alamofire.Method {
    return .GET
  }
  
  var URLRequest: NSMutableURLRequest {
    let (path, parameters): (String, [String : AnyObject]?) = {
      
      switch self {
      case .GetAllGames():
        return ("/games", nil)
      }
      
    }()
    
    let url = NSURL(string: NetworkRouter.baseUrl)
    let urlRequest = NSMutableURLRequest(URL: url!.URLByAppendingPathComponent(path))
    urlRequest.HTTPMethod = method.rawValue
    let encoding = Alamofire.ParameterEncoding.JSON
    return encoding.encode(urlRequest, parameters: parameters).0
  }
  
}
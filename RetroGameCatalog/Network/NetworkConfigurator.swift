//
//  NetworkConfigurator.swift
//  RetroGameCatalog
//
//  Created by Justin Wright on 2/11/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import Alamofire

class NetworkConfigurator {
  
  static let sharedInstance = NetworkConfigurator()
  
  let manager: Alamofire.Manager = {
    
    //let serverTrustPolicies: [String: ServerTrustPolicy] = nil
    let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
    configuration.HTTPAdditionalHeaders = Alamofire.Manager.defaultHTTPHeaders
    configuration.timeoutIntervalForResource = 10 // seconds
    
    configuration.HTTPAdditionalHeaders?.updateValue("application/json",
      forKey: "Content-Type")
    
    return Alamofire.Manager(configuration: configuration, serverTrustPolicyManager: nil)
    
  }()
  
}
//
//  ResponseImage.swift
//  RetroGameCatalog
//
//  Created by Justin Wright on 2/11/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import Alamofire

extension Request {
  
  public static func imageResponseSerializer() -> ResponseSerializer<UIImage, NSError> {
    
    return ResponseSerializer { request, response, data, error in
      guard error == nil else { return .Failure(error!) }
      
      if let image = UIImage(data: data!, scale: UIScreen.mainScreen().scale) {
        return .Success(image)
      }
      
      let error = Error.errorWithCode(.DataSerializationFailed, failureReason: "Could not create an image from data")
      return .Failure(error)
    }
  }
  
  public func responseImage(completionHandler: Response<UIImage, NSError> -> Void) -> Self {
    return response(responseSerializer: Request.imageResponseSerializer(), completionHandler: completionHandler)
  }
}
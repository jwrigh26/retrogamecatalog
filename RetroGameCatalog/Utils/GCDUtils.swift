//
//  GCDUtils.swift
//  RetroGameCatalog
//
//  Created by Justin Wright on 9/11/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import Foundation

var GlobalMainQueue: dispatch_queue_t {
  return dispatch_get_main_queue()
}


/*
* GlobalUserInteractiveQueue
*
* A QOS class which indicates work performed by this thread
* is interactive with the user.
*/
var GlobalUserInteractiveQueue: dispatch_queue_t {
  return dispatch_get_global_queue(Int(QOS_CLASS_USER_INTERACTIVE.rawValue), 0)
}

/*
* GlobalUserInitiatedQueue
*
* A QOS class which indicates work performed by this thread
* was initiated by the user and that the user is likely waiting for the
* results.

* Example

dispatch_async(GlobalUserInteractiveQueue, { () in

})

*/
var GlobalUserInitiatedQueue: dispatch_queue_t {
  return dispatch_get_global_queue(Int(QOS_CLASS_USER_INITIATED.rawValue), 0)
}


/*
* GlobalUtilityQueue
*
* A QOS class which indicates work performed by this thread
* may or may not be initiated by the user and that the user is unlikely to be
* immediately waiting for the results.
*/
var GlobalUtilityQueue: dispatch_queue_t {
  return dispatch_get_global_queue(Int(QOS_CLASS_UTILITY.rawValue), 0)
}

/*
* GlobalBackgroundQueue
*
* A QOS class which indicates work performed by this thread was not
* initiated by the user and that the user may be unaware of the results.
*/
var GlobalBackgroundQueue: dispatch_queue_t {
  return dispatch_get_global_queue(Int(QOS_CLASS_BACKGROUND.rawValue), 0)
}


func createSerialQueue(name: String)-> dispatch_queue_t {
  return dispatch_queue_create(name, DISPATCH_QUEUE_SERIAL)
}

func createConCurrentQueue(name: String)-> dispatch_queue_t {
  return dispatch_queue_create(name, DISPATCH_QUEUE_CONCURRENT)
}

func delay(delay:Double, closure:()->()) {
  dispatch_after(
    dispatch_time(
      DISPATCH_TIME_NOW,
      Int64(delay * Double(NSEC_PER_SEC))
    ),
    GlobalMainQueue, closure)
}

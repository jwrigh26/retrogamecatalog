//
//  AppDelegate.swift
//  RetroGameCatalog
//
//  Created by Justin Wright on 2/11/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit
import CoreData

typealias BasicHandler = () -> Void // For this example, not sure where to put this..so yeah throw it in the AppDelegate.

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  var coreDataStack: CoreDataStack!

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    application.statusBarStyle = .LightContent // Just a visual enhancment for the demo
    
    coreDataStack = CoreDataStack { [weak self] in
      guard let stack = self?.coreDataStack else { return }
      GameData.sharedInstance.seedData(stack) {
        // Let the Root View Controller know we have a data stack now.
        self?.coreDataStack.assignCoreDataStackToRoot(self?.window)
      }
    }
    
    return true
  }

  func applicationWillResignActive(application: UIApplication) {
    coreDataStack.save()
  }

  func applicationDidEnterBackground(application: UIApplication) {
    coreDataStack.save()
  }

  func applicationWillEnterForeground(application: UIApplication) {
  }

  func applicationDidBecomeActive(application: UIApplication) {
  }

  func applicationWillTerminate(application: UIApplication) {
    coreDataStack.save()
  }


}


//
//  CoreDataStack.swift
//  RetroGameCatalog
//
//  Created by Justin Wright on 1/26/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation


import Foundation
import UIKit
import CoreData

enum DataError: ErrorType {
  case General
  case Import
  case Fetch
  case Delete
}



final class CoreDataStack: NSObject {
  
  static let moduleName = "RetroGameCatalog"
  
  var handler: BasicHandler?
  
  init(handler: BasicHandler?) {
    self.handler = handler
    super.init()
    let _ = self.managedObjectContext
  }
  
  func assignCoreDataStackToRoot(window: UIWindow?){
    // Pass the Baton!
    if let root = window?.rootViewController as? UINavigationController {
      if let top = root.topViewController where top.respondsToSelector("setCoreDataStack:") {
        top.performSelector("setCoreDataStack:", withObject: self)
      }
    }
  }
  
  
  func save() {
    guard managedObjectContext.hasChanges || privateManagedObjectContext.hasChanges else {
      return
    }
    
    managedObjectContext.performBlockAndWait() {
      do {
        try self.managedObjectContext.save()
      } catch {
        fatalError("Error saving main managed object context! \(error)")
      }
    }
    
    privateManagedObjectContext.performBlock() {
      do {
        try self.privateManagedObjectContext.save()
      } catch {
        fatalError("Error saving private managed object context! \(error)")
      }
    }
    
  }
  
  func saveChildContext(child: NSManagedObjectContext){
    guard child.hasChanges else { return }
    do {
      try child.save()
      self.save()
    } catch {
      fatalError("Error saving child context \(error)")
    }
    
    
  }
  
  func spawnChildContext(context: NSManagedObjectContext, isPrivate: Bool = false) -> NSManagedObjectContext{
    
    let concurrencyType: NSManagedObjectContextConcurrencyType = isPrivate ? .PrivateQueueConcurrencyType : .MainQueueConcurrencyType
    let child = NSManagedObjectContext(concurrencyType: concurrencyType)
    child.parentContext = context
    return child
  }
  
  lazy var managedObjectModel: NSManagedObjectModel = {
    let modelURL = NSBundle.mainBundle().URLForResource(moduleName, withExtension: "momd")!
    return NSManagedObjectModel(contentsOfURL: modelURL)!
  }()
  
  lazy var applicationDocumentsDirectory: NSURL = {
    return NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).last!
  }()
  
  lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
    
    let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
    let persistentStoreURL = self.applicationDocumentsDirectory.URLByAppendingPathComponent("\(moduleName).sqlite")
    
    // This could take a while so we return right away but notify when this is done laters.
    // http://martiancraft.com/blog/2015/03/core-data-stack/
    dispatch_async(GlobalBackgroundQueue){ [weak self] in
      
      do {
        try coordinator.addPersistentStoreWithType(NSSQLiteStoreType,
          configuration: nil,
          URL: persistentStoreURL,
          options: [NSMigratePersistentStoresAutomaticallyOption: true,
            NSInferMappingModelAutomaticallyOption: false])
        if self?.handler != nil {
          self?.handler?()
        }
      } catch {
        fatalError("Persistent store error! \(error)")
      }
      
    }
    
    return coordinator
  }()
  
  private lazy var privateManagedObjectContext: NSManagedObjectContext = {
    let moc = NSManagedObjectContext(concurrencyType: .PrivateQueueConcurrencyType)
    moc.persistentStoreCoordinator = self.persistentStoreCoordinator
    return moc
  }()
  
  lazy var managedObjectContext: NSManagedObjectContext = {
    let managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
    managedObjectContext.parentContext = self.privateManagedObjectContext
    return managedObjectContext
  }()
  
}
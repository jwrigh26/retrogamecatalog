//
//  GameData.swift
//  RetroGameCatalog
//
//  Created by Justin Wright on 2/11/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import CoreData

final class GameData: NSObject {
  
  typealias gameDataHandler = (localItems: Set<LocalGame>?) -> Void
  
  static let sharedInstance = GameData()
  static let catalogName = "My Nintendo Catalog"
  var catalog: LocalCatalog?
  
  // MARK: Seed
  
  func seedData(stack: CoreDataStack, handler: BasicHandler) {
    let childContext = stack.spawnChildContext(stack.managedObjectContext, isPrivate: true)
    childContext.performBlock{ [weak self] in
      
      do {
        // Set up shop
        try self?.importCatalog(childContext, name: GameData.catalogName)
        // Delete
        try self?.deleteGames(childContext)
        
        
        
        
        // Import
        self?.importAllGames(childContext) { games in
          if let games = games {
            self?.catalog?.games = games
          }
          
          dispatch_async(GlobalMainQueue){
            stack.saveChildContext(childContext)
            handler()
          }
          
        }
      }catch {
        print("Error Seeding Data \(error)")
        childContext.rollback()
        handler()
      }
    }
  }
  
  // MARK: Fetch
  
  func fetchAllGames(context: NSManagedObjectContext, router: DataRouter) throws -> [LocalGame]{
    var items = [LocalGame]()
    do {
      let results = try context.executeFetchRequest(router.Request) as! [LocalGame]
      if !results.isEmpty {
        items = results
      }
    }catch let error as NSError{
      print("Local Game Fetch Error: \(error) and description \(error.description)")
      throw DataError.Fetch
      
    }
    return items
  }
  
  func fetchCatalog(context: NSManagedObjectContext, name: String = GameData.catalogName) throws -> LocalCatalog? {
    let fetch = DataRouter.catalog(context: context, name: name)
    var catalog: LocalCatalog? = nil
    do {
      let results = try context.executeFetchRequest(fetch.Request) as! [LocalCatalog]
      if !results.isEmpty {
        catalog = results.first
      }
    }catch let error as NSError {
      print("Local Catalog Fetch Error: \(error) and description \(error.description)")
      throw DataError.Fetch
    }
    return catalog
  }
  
  
  // MARK: Import
  
  func importCatalog(context: NSManagedObjectContext, name: String) throws {
    do {
      if let localCatalog = try fetchCatalog(context, name: name) {
        print("We have a Catalog saved already named \(localCatalog.name) and games already found \(localCatalog.games.count)")
        catalog = localCatalog
      }else{
        let entity = NSEntityDescription.entityForName("LocalCatalog", inManagedObjectContext: context)
        catalog = LocalCatalog(entity: entity!, insertIntoManagedObjectContext: context)
        catalog?.name = name
        if let catalog = self.catalog {
          print("We are creating a local catalog called: \(catalog.name)")
        }
        
      }
    }catch let error as NSError {
      print("Local Catalog Import Error: \(error) and description \(error.description)")
      throw DataError.Import
    }
  }
  
  func importAllGames(context: NSManagedObjectContext, handler: gameDataHandler) {
    
    NetworkManager.getItems{ [weak self] response, error in
      guard let games = response as? [Game] else {
        handler(localItems: nil)
        return
      }
      
      let set = self?.importLocalGames(context, games: games)
      handler(localItems: set)
    }
  }
  
  func importLocalGames(context: NSManagedObjectContext, games: [Game]) -> Set<LocalGame> {
    let entity = NSEntityDescription.entityForName("LocalGame", inManagedObjectContext: context)
    var set = Set<LocalGame>()
    
    for index in 0..<games.count {
      let game = games[index];
      let localGame = LocalGame(entity: entity!, insertIntoManagedObjectContext: context)
      
      localGame.name = game.name
      localGame.developer = game.developer
      localGame.year = game.date
      
      set.insert(localGame)
    }
    return set
  }
  
  
  
  // MARK: Delete
  
  func deleteGames(context: NSManagedObjectContext) throws {
    let fetch = DataRouter.allGames(context: context)
    do {
      let results = try context.executeFetchRequest(fetch.Request) as! [LocalGame]
      for result in results {
        context.deleteObject(result)
      }
    }catch let error as NSError{
      print("Delete Games Error: \(error) and description \(error.description)")
      throw DataError.Delete
    }
  }
  
  func deleteCatalog(context: NSManagedObjectContext, name: String) throws {
    let fetch = DataRouter.catalog(context: context, name: name)
    do {
      let results = try context.executeFetchRequest(fetch.Request) as! [LocalCatalog]
      for result in results { context.deleteObject(result); print("Deleting object"); }
    }catch let error as NSError {
      print("Delete Catalog Error: \(error) and description \(error.description)")
      throw DataError.Delete
    }
  }
}
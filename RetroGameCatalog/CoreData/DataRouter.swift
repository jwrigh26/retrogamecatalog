//
//  DataRouter.swift
//  RetroGameCatalog
//
//  Created by Justin Wright on 2/8/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import CoreData

enum DataRouter {
  
  case catalog(context: NSManagedObjectContext, name: String)
  case game(context: NSManagedObjectContext, name: String)
  case allGames(context: NSManagedObjectContext)
  case gamesFromDeveloper(context: NSManagedObjectContext, developer: String)
  case gamesInYear(context: NSManagedObjectContext, year: String)
  
  
  var Request: NSFetchRequest {
    let (context, description, predicate): (NSManagedObjectContext, String, NSPredicate?) = {
      switch self {
      case .catalog(let context, let name):
        let predicate = NSPredicate(format: "name == %@", name)
        return (context, "LocalCatalog", predicate)
      case .game(let context, let name):
        let predicate = NSPredicate(format: "name == %@", name)
        return (context, "LocalGame", predicate)
        
      case .allGames(let context):
        return (context, "LocalGame", nil)
        
      case .gamesFromDeveloper(let context, let developer):
        let predicate = NSPredicate(format: "developer == %@", developer)
        return (context, "LocalGame", predicate)
        
      case .gamesInYear(let context, let year):
        let predicate = NSPredicate(format: "date == %@", year)
        return (context, "LocalGame", predicate)
      }
      
    }()
    
    let entity = NSEntityDescription.entityForName(description, inManagedObjectContext: context)
    let fetch = NSFetchRequest()
    fetch.entity = entity
    if let predicate = predicate {
      fetch.predicate = predicate
    }
    return fetch
  }
  
}
//
//  ViewController.swift
//  RetroGameCatalog
//
//  Created by Justin Wright on 2/11/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit



final class CatalogViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  
  @IBOutlet var tableView: UITableView!
  @IBOutlet var indicator: UIActivityIndicatorView!
  
  var coreDataStack: CoreDataStack! {
    didSet {
      print("Time to fetch games")
      fetchGames()
    }
  }
  
  var games = [LocalGame]()

  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.title = "Game Catalog"
    indicator.startAnimating()
    
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }

  func fetchGames(){
    // We could do this another way, by just getting the singleton's reference to catalog,
    // but it's good practice to show how to fetch from the main context too.
    do {
      if coreDataStack != nil {
        let catalog = try GameData.sharedInstance.fetchCatalog(coreDataStack.managedObjectContext)
        print("Catalog fetched is \(catalog?.name)")
        if let games = catalog?.games.allObjects as? [LocalGame] {
          self.games = games
        }
      }
    }catch{
      print("Fetching Games Failed: \(error)")
    }
    
    dispatch_async(GlobalUserInteractiveQueue){ [weak self] in
      if let games = self?.games {
        self?.games = games.sort { $0.name < $1.name }
      }
      
      dispatch_async(GlobalMainQueue){
        self?.tableView.reloadData()
        self?.indicator.stopAnimating()
      }
      
    }
  }
  
  // MARK: DataSource and Delegate for TableView
  // I don't always creaete a TableView datasource and delegate, but when I do... I throw it in the ViewController
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return games.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("CatalogCell", forIndexPath: indexPath)
    let game = games[indexPath.row]
    cell.textLabel?.text = game.displayName
    cell.detailTextLabel?.text = game.year
    
    return cell
  }

}


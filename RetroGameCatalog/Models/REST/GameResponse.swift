//
//  GameItemResponse.swift
//  RetroGameCatalog
//
//  Created by Justin Wright on 2/11/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import UIKit

final class GameResponse: NSObject, ResponseObjectSerializable, JMWPojo {
  
  private(set) var message : String!
  private(set) var games : [Game]!
  
  required init?(response: NSHTTPURLResponse, representation: AnyObject) {
    super.init()
    self.message = valueForStringOptionalAtKeyPath(representation, path: "message")
    
    var array: [Game]?
    
    if let object: AnyObject = representation.valueForKeyPath("games") {
      array = Game.collection(response: response, representation: object)
    }
    
    if let games = array {
      self.games = games
    }else{
      self.games = [Game]()
    }
    
  }
  
}

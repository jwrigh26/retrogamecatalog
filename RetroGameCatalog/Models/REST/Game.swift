//
//  Game.swift
//  RetroGameCatalog
//
//  Created by Justin Wright on 2/11/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import UIKit

final class Game: NSObject, NSCoding, ResponseObjectSerializable, JMWPojo {
  
  private(set) var name: String!
  private(set) var developer: String!
  private(set) var date: String!
  
  
  static func collection(response response: NSHTTPURLResponse, representation: AnyObject) -> [Game] {
    guard let array = representation as? [AnyObject]
      else {
        print("tracker representation failed");
        return [Game]()
    }
    var items = [Game]()
    for r in array {
      if let i = Game(response: response, representation: r) {
        items.append(i)
      }
    }
    return items
  }
  
  required init?(response: NSHTTPURLResponse, representation: AnyObject) {
    super.init()
    self.name = valueForStringOptionalAtKeyPath(representation, path: "name")
    self.developer = valueForStringOptionalAtKeyPath(representation, path: "developer")
    self.date = valueForStringOptionalAtKeyPath(representation, path: "date")
    
  }
  
  init(coder aDecoder: NSCoder) {
    self.name = aDecoder.decodeObjectForKey("name") as! String
    self.developer = aDecoder.decodeObjectForKey("developer") as! String
    self.date = aDecoder.decodeObjectForKey("date") as! String
    
  }
  
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeObject(self.name, forKey: "name")
    aCoder.encodeObject(self.developer, forKey: "developer")
    aCoder.encodeObject(self.date, forKey: "date")
  }
  
}

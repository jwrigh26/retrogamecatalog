//
//  JMWPojo.swift
//  RetroGameCatalog
//
//  Created by Justin Wright on 2/11/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation

protocol JMWPojo {
  
  func valueForStringOptional(value: String?) -> String
  func valueForStringOptionalAtKeyPath(representation: AnyObject, path: String) -> String
  func valueForIntOptional(value: Int?) -> Int
}

extension JMWPojo {
  
  func valueForStringOptional(value: String?) -> String {
    if let string = value {
      return string
    }else{
      return ""
    }
  }
  
  func valueForIntOptional(value: Int?) -> Int {
    if let number = value {
      return number
    }else{
      return 0
    }
  }
  
  func valueForFloatOptional(value: Float?) -> Float {
    if let number = value {
      return number
    } else {
      return 0
    }
  }
  
  func valueForStringOptionalAtKeyPath(representation: AnyObject, path: String) -> String {
    if let string = representation.valueForKeyPath(path) as? String {
      return string
    }
    else {
      return ""
    }
  }
  
  func valueForDictionaryAtKeyPath(representation: AnyObject, path: String) -> [String: String] {
    if let dic = representation.valueForKeyPath(path) as? [String: String] {
      return dic
    }else{
      return ["" : ""]
    }
  }
  
  func valueForBoolOptionalAtKeyPath(representation: AnyObject, path: String) -> Bool {
    if let bool = representation.valueForKeyPath(path) as? Bool {
      return bool
    }
    return false
  }
  
  func valueForFloatOptionalAtKeypath(representation: AnyObject, path: String) -> Float {
    if let number = representation.valueForKeyPath(path) as? Float {
      return number
    }
    return 0.0
  }
  
}
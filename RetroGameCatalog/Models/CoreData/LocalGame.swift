//
//  LocalGame.swift
//  RetroGameCatalog
//
//  Created by Justin Wright on 2/11/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import CoreData


class LocalGame: NSManagedObject {

  var displayName: String {
    return "\(name) by \(developer)"
  }

}

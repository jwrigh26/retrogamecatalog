//
//  LocalCatalog+CoreDataProperties.swift
//  RetroGameCatalog
//
//  Created by Justin Wright on 2/11/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension LocalCatalog {

    @NSManaged var name: String
    @NSManaged var games: NSSet

}
